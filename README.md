# BITEhack 2019

Hackathon organized by BEST AGH, January 2019

My team made an extraordinary toy for kids (or students), which was a fluffy teddy bear called Pysio. He can tell funny jokes and play games. 

We won the special award for the most entertainment robot.

## Hardware

Raspberry Pi 3B+

Arduino Leonardo
